<?php
    require_once "animal.php";
    require_once "ape.php";
    require_once "frog.php";

    $sheep = new Animal("shaun");

    echo "Name :".$sheep->name."<br>"; // "shaun"
    echo "Legs :".$sheep->legs."<br>"; // 4
    echo "cold_blooded :".$sheep->cold_blooded."<br>"; // "no"
    
    // NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

    echo "<br>";

    $sungokong = new Ape("kera sakti");
    echo "Name :".$sungokong->name."<br>"; 
    echo "Legs :".$sungokong->legs."<br>"; 
    echo "cold_blooded :".$sungokong->cold_blooded."<br>";
    echo "yell :";
    $sungokong->yell(); // "Auooo"
    echo "<br>";
    $kodok = new Frog("buduk");
    echo "Name :".$kodok->name."<br>"; 
    echo "Legs :".$kodok->legs."<br>"; 
    echo "cold_blooded :".$kodok->cold_blooded."<br>";
    echo "jump :" ;
    $kodok->jump() ; // "hop hop"

?>